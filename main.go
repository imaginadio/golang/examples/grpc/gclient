package main

import (
	"context"
	"fmt"
	"os"

	p "gitlab.com/imaginadio/golang/examples/grpc/protobuf"
	"google.golang.org/grpc"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("usage: %s port_number", os.Args[0])
		return
	}

	port := ":" + os.Args[1]

	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	gclient := p.NewMsgServiceClient(conn)
	resp, err := gclient.GiveMeResp(ctx, &p.Request{
		Text:    "hello from client",
		Subtext: "sup",
	})

	if err != nil {
		panic(err)
	}

	fmt.Println("respose:", resp)
}
